import requests as r
from bs4 import BeautifulSoup as BS
import os


def construct_url(url, i):
    if i==1:
        return url
    else:
        return url[:-5]+f"_{i}.html"



def get_pic_url(url):
    resp = r.get(url)
    if resp.status_code == 200:
        resp = resp.text.encode(resp.encoding).decode('gbk')
        soup = BS(resp)
        find = soup.find_all('div', {"id": "disappear"})[0].a
        if find != None:
            img_url = "http://www.fclrzp.com"+find["href"]
        else:
            return None
        title = soup.title.text
        return (img_url, title)
    else:
        return None


def save_pic(url, loc, i, root):
    img = r.get(url)
    if img.status_code == 200:
        with open(root+f"/{loc}/a_{i}.jpg", "wb") as f:
            f.write(img.content)


def loop_in_album(index, url, root):
    i = 1
    if not os.path.exists(root+f"/{index}"):
        os.mkdir(root+f"/{index}")
    else:
        return
    url_item = construct_url(url, i)
    result = get_pic_url(url_item)
    if result == None:
        return
    with open(root+f"/{index}/description", "w") as f:
        f.write(result[1])
    while result:
        save_pic(result[0], index, i, root)
        i += 1
        url_item = construct_url(url, i)
        result = get_pic_url(url_item)
        print(i)
    


def construct_url_list(index):
    d = {1: "zhongguo", 2: "rihan", 3: "oumei", 5: "meinv", 6: "zipai"}
    i = 1
    l = []
    url = f"http://www.fclrzp.com/{d[index]}/list_{index}_{i}.html"
    resp = r.get(url)
    while resp.status_code == 200:
        resp = resp.text.encode(resp.encoding).decode('gbk')
        soup = BS(resp)
        print(i)
        for li in soup.find_all("div",{"class": "list"})[0].ul.find_all("li"):
            l.append("http://www.fclrzp.com"+li.find("a")["href"])
        i += 1
        url = f"http://www.fclrzp.com/{d[index]}/list_{index}_{i}.html"
        resp = r.get(url)
    return l


def download_all():
    d = {1: "zhongguo", 2: "rihan", 3: "oumei", 5: "meinv", 6: "zipai"}
    for k in d.keys():
        if not os.path.exists(f"{d[k]}"):
            os.mkdir(f"{d[k]}")
        ls = construct_url_list(k)
        for i, link in enumerate(ls):
            loop_in_album(i, link, d[k])


download_all()
